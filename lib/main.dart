import 'package:dropdown_app/list_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {

  ListProvider.instance.getListItems();
  runApp(MyApp());

}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {


    return ChangeNotifierProvider(
      create: (ctx) => ListProvider.instance,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String text = 'set item';
  @override
  Widget build(BuildContext context) {

    var itemData = Provider.of<ListProvider>(context);
    final listItems = itemData.listItems;
    // print('listItems.length ${listItems.length}');
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              text,
            ),
            DropdownButton<String>(
              items: listItems.map((String value) {
                print('DropdownMenuItem $value');
                return DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
              onChanged: (newVal){
                text = newVal;
                this.setState(() {});
              },
            )
          ],
        ),
      ),
    );
  }
}
